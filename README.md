# Sandklef ScanCode Docker Image

Docker image for running ScanCode (https://github.com/nexB/scancode-toolkit) by nexB (https://www.nexb.com/) in a docker image.

# Background

In my job (with license compliance) I am frequently asking colleagues to scan some code, using ScanCode, and send me the report. This usually means my colleague needs to install ScanCode and its dependencies. This usually takes a while and sometimes we stumble across some version conflict. So I was suggested to create a docker image (all developers at my work is using docker). And here we are.

# Download the actual image

Actual image can be found here: https://hub.docker.com/repository/docker/sandklef/sandklef-scancode

```
docker pull sandklef/sandklef-scancode
```

# Use the image

Let's assume you want to scan the current directory


```
docker run -it -v `pwd`/:/code-to-scan:z sandklef/sandklef-scancode

```

... the scan will run for a while. Once done, you should see reports in the directory scan-reports/. The report is created in JSON, HTML, SPDX (RDF and TV) formats.
